#pragma once
/** @file */

#ifndef EXP_MM_H
#define EXP_MM_H


#include "../experiments/experiment.h"


namespace ksi
{
  
    class exp_MM : virtual public experiment
    {
    public:
        /** The method executes an experiment. */
        virtual void execute() ;
        virtual ~exp_MM() {}
    };
}

#endif 