/** @file */

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>


#include "../readers/reader-complete.h" 
#include "../common/dataset.h" 
#include "../partitions/partitioner.h" 
#include "../partitions/fcm.h"
#include "../partitions/fcom.h"
#include "../partitions/fcm-conditional.h"
#include "../partitions/fcm-possibilistic.h"
#include "../partitions/sfcm.h" 
#include "../dissimilarities/dis-log.h"
#include "../dissimilarities/dis-log-linear.h"
#include "../dissimilarities/dis-huber.h"
#include "../dissimilarities/dis-sigmoidal.h"
#include "../dissimilarities/dis-sigmoidal-linear.h"
#include "../owas/sowa.h"
#include "../owas/plowa.h"
#include "../owas/uowa.h"
#include "../service/debug.h"

#include "../experiments/MIchalMExp.h"
#include"MIchalMExp.h"
#include <fstream>

void ksi::exp_MM::execute() {

	try
	{
        {
            std::string dataDir("C:\\Users\\RetailAdmin\\Desktop\\Studia\\MGR\\NTWI\\NtWi\\dataNTWI.txt");
            const double EPSILON = 1e-8;
            const int NUMBER_OF_CLUSTERS = 2;
            ksi::reader_complete input;
            auto DataSet = input.read(dataDir);

            
            ksi::fcm algorithm;
            algorithm.setEpsilonForFrobeniusNorm(EPSILON);
            algorithm.setNumberOfClusters(NUMBER_OF_CLUSTERS);

            auto Partition = algorithm.doPartition(DataSet);

            std::cout << "FCM" << std::endl;
            std::cout << "===" << std::endl;
            std::cout << Partition << std::endl;
            std::cout << std::endl;
        }
	}
    CATCH;
    return;
}